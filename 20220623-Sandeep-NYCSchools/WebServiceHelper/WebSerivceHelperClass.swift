//
//  ViewController.swift
//  20220623-Sandeep-NYCSchools
//

import Foundation
import Alamofire



class WebSerivceHelperClass {

    // MARK: - GET high school data
    static func fetchHighSchoolData(completion : @escaping (Bool, highSchoolModel?, String) -> Void) {
        
        let headers : HTTPHeaders = ["content-type": "application/json"]
        let apiURL = UtilityDataEnum.serviceURL + UtilityDataEnum.highSchoolURL
        
        AF.request(apiURL, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
            .responseJSON { response in
                switch response.result {
                case .success(let value):
                    do {
                        let highschoolres = try JSONDecoder().decode(highSchoolModel.self, from: response.data!)
                        //print(highschoolres)
                        completion(true, highschoolres, "")
                        
                    } catch let error as NSError {
                        print("Failed to load: \(error.localizedDescription)")
                        completion(false, nil, "\(error.localizedDescription)")
                    }
                    
                case .failure(let error):
                    //print(error)
                    var errorMsg = ""
                    if let underlyingError = error.underlyingError {
                        if let urlError = underlyingError as? URLError {
                            switch urlError.code {
                                case .timedOut:
                                    print("Timed out error")
                                    errorMsg = "Connection Timed out error"
                                case .notConnectedToInternet:
                                    print("Not connected")
                                    errorMsg = "The Internet connection appears to be offline."
                                default:
                                    //Do something
                                    print("Unmanaged error")
                                    errorMsg = error.localizedDescription
                                }
                        }
                    }
                    completion(false, nil, errorMsg)
                }
            }
    }

    // MARK: - GET SAT Score data
    static func fetchSATScoreData(completion : @escaping (Bool, satScoredata?, String) -> Void) {
        
        let headers : HTTPHeaders = ["content-type": "application/json"]
        let apiURL = UtilityDataEnum.serviceURL + UtilityDataEnum.satScoreURL
        
        AF.request(apiURL, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
            .responseJSON { response in
                switch response.result {
                case .success(let value):
                    do {
                        let satscoreres = try JSONDecoder().decode(satScoredata.self, from: response.data!)
                        //print(satscoreres)
                        completion(true, satscoreres, "")
                        
                    } catch let error as NSError {
                        print("Failed to load: \(error.localizedDescription)")
                        completion(false, nil, "\(error.localizedDescription)")
                    }
                    
                case .failure(let error):
                    //print(error)
                    var errorMsg = ""
                    if let underlyingError = error.underlyingError {
                        if let urlError = underlyingError as? URLError {
                            switch urlError.code {
                                case .timedOut:
                                    print("Timed out error")
                                    errorMsg = "Connection Timed out error"
                                case .notConnectedToInternet:
                                    print("Not connected")
                                    errorMsg = "The Internet connection appears to be offline."
                                default:
                                    //Do something
                                    print("Unmanaged error")
                                    errorMsg = error.localizedDescription
                                }
                        }
                    }
                    completion(false, nil, errorMsg)
                }
            }
    }
    
}

