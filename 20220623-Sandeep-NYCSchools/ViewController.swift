//
//  ViewController.swift
//  20220623-Sandeep-NYCSchools
//
//  Created by Sandeep on 6/23/22.
//

import UIKit

class ViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    
    @IBOutlet weak var schoolTblIb: UITableView!
    @IBOutlet weak var searchBarIb: UISearchBar!
    
    
    var schoolData = [[String:Any]]()
    var searchedSchool = [[String:Any]]()
    var searching = false
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.searchBarIb.barTintColor = UIColor.colorFromHex("#221d4c")
        self.searchBarIb.tintColor = UIColor.white
        self.searchBarIb.showsCancelButton = true
        
        let searchTextField = self.searchBarIb.searchTextField
        searchTextField.textColor = UIColor.blue
        searchTextField.clearButtonMode = .never
        searchTextField.backgroundColor = UIColor.white
        
        let glassIconView = searchTextField.leftView as! UIImageView
        glassIconView.image = glassIconView.image?.withRenderingMode(.alwaysTemplate)
        glassIconView.tintColor = UIColor.blue
        
        //self.fetchHighSchoolData()
    }

    // MARK: - UITableView delegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searching {
            return searchedSchool.count
        } else {
            return schoolData.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.schoolTblIb.dequeueReusableCell(withIdentifier: "schollCellIdentifier", for: indexPath) as! SchollCell
        
        cell.selectionStyle = .none
        
        var schoolNamestr = ""
        var schoolPhonestr = ""
        var schoolEmailstr = ""
        
        if searching {
            schoolNamestr = (searchedSchool[indexPath.row]["school_name"] as? String) ?? ""
            schoolPhonestr = (searchedSchool[indexPath.row]["phone_number"] as? String) ?? ""
            schoolEmailstr = (searchedSchool[indexPath.row]["school_email"] as? String) ?? ""
        } else {
            schoolNamestr = (schoolData[indexPath.row]["school_name"] as? String) ?? ""
            schoolPhonestr = (schoolData[indexPath.row]["phone_number"] as? String) ?? ""
            schoolEmailstr = (schoolData[indexPath.row]["school_email"] as? String) ?? ""
        }
        
        cell.schoolNamelblIb.text = schoolNamestr
        cell.schoolPhoneIb.text = schoolPhonestr
        cell.schoolEmailIb.text = schoolEmailstr
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let mainBoard = UIStoryboard(name: "Main", bundle: nil)
        let detailVC = mainBoard.instantiateViewController(withIdentifier: "detailViewControllerScene") as! DetailViewController
        if searching {
            detailVC.schoolDetaildata = searchedSchool[indexPath.row]
        } else {
            detailVC.schoolDetaildata = schoolData[indexPath.row]
        }
        detailVC.modalPresentationStyle = .fullScreen
        self.topMostController().present(detailVC, animated: true, completion: nil)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 95
    }
    
    func fetchHighSchoolData() {
        
        self.showIndicator(withTitle: "Please wait", and: "")
        
        WebSerivceHelperClass.fetchHighSchoolData{ [weak self] (success, highschoolData, message) in
            guard let weakself = self else {
                return
            }
            if success == true {
                AppData.shared.highschooldata = highschoolData
                if let highschoolitems = highschoolData {
                    for eachItem in highschoolitems {
                        var data = [String: Any]()
                        for eachData in eachItem {
                            let key = eachData.key
                            if key == "school_name" {
                                data[eachData.key] = eachData.value
                            } else if key == "school_email" {
                                data[eachData.key] = eachData.value
                            } else if key == "phone_number" {
                                data[eachData.key] = eachData.value
                            }
                        }
                        weakself.schoolData.append(data)
                    }
                }
                weakself.hideIndicator()
                weakself.schoolTblIb.reloadData()
                
            } else {
                weakself.hideIndicator()
                weakself.displayAlert(dispmessage: message)
            }
        }
    }

}


extension ViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchedSchool = schoolData.filter{($0["school_name"] as? String ?? "").lowercased().prefix(searchText.count) == searchText.lowercased()}
        searching = true
        schoolTblIb.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder() // hides the keyboard.
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searching = false
        searchBar.text = ""
        self.searchBarIb.searchTextField.endEditing(true)
        schoolTblIb.reloadData()
    }
}
