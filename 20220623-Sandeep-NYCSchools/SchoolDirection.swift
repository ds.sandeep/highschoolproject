//
//  SchoolDirection.swift
//  20220623-Sandeep-NYCSchools
//

import UIKit
import MapKit
import CoreLocation



class SchoolDirection: BaseViewController, MKMapViewDelegate {

    
    
    @IBOutlet weak var backBtnIb: UIButton!
    @IBOutlet weak var mapkitIb: MKMapView!
    
    
    
    let locationManager = CLLocationManager()
    var currentLoc: CLLocation!
    var schoolNamestr = ""
    var schoollatstr = ""
    var schoollongstr = ""
    var schoolNeighborhoodstr = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        checkLocationServices()
        
        let schoolAnn = SchoolAnn(title: schoolNamestr, coordinate: CLLocationCoordinate2D(latitude: (Double(schoollatstr) ?? 0.00), longitude: (Double(schoollongstr) ?? 0.00)), info: schoolNeighborhoodstr)
        
        mapkitIb.showsCompass = true
        mapkitIb.isZoomEnabled = true
        mapkitIb.addAnnotation(schoolAnn)
        
        let loc1 = CLLocationCoordinate2D.init(latitude: 40.741895, longitude: -73.989308)
        //let loc1 = CLLocationCoordinate2D.init(latitude: currentLoc.coordinate.latitude, longitude: currentLoc.coordinate.longitude)
        let loc2 = CLLocationCoordinate2D.init(latitude: (Double(schoollatstr) ?? 0.00), longitude: (Double(schoollongstr) ?? 0.00))

        //find route
        showRouteOnMap(currentCoordinate: loc1, destinationCoordinate: loc2)
    }

    // MARK: - back button action
    @IBAction func backButtonAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func checkLocationServices() {
      if CLLocationManager.locationServicesEnabled() {
        checkLocationAuthorization()
      } else {
        // Show alert letting the user know they have to turn this on.
      }
    }
    
    func checkLocationAuthorization() {
        
      switch CLLocationManager.authorizationStatus() {
      case .authorizedWhenInUse,
           .authorizedAlways:
          currentLoc = locationManager.location
          mapkitIb.showsUserLocation = true
       case .denied: // Show alert telling users how to turn on permissions
          self.displayAlert(dispmessage: "Location: Access is denied, please enable it in settings.")
       break
      case .notDetermined:
          locationManager.requestWhenInUseAuthorization()
          mapkitIb.showsUserLocation = true
      case .restricted: // Show an alert letting them know what’s up
       break
      @unknown default:
          break
      }
    }
    
    func showRouteOnMap(currentCoordinate: CLLocationCoordinate2D, destinationCoordinate: CLLocationCoordinate2D) {

        let request = MKDirections.Request()
        
        request.source = MKMapItem(placemark: MKPlacemark(coordinate: currentCoordinate, addressDictionary: nil))
        request.destination = MKMapItem(placemark: MKPlacemark(coordinate: destinationCoordinate, addressDictionary: nil))
        
        request.requestsAlternateRoutes = true
        request.transportType = .automobile

        let directions = MKDirections(request: request)

        directions.calculate { [unowned self] response, error in
            guard let unwrappedResponse = response else {
                print("Error calculating direction request \(String(describing: error))")
                self.displayAlert(dispmessage: error?.localizedDescription ?? "Error displaying direction")
                return
            }
            
            //for getting just one route
            if let route = unwrappedResponse.routes.first {
                //show on map
                self.mapkitIb.addOverlay(route.polyline)
                //set the map area to show the route
                self.mapkitIb.setVisibleMapRect(route.polyline.boundingMapRect, edgePadding: UIEdgeInsets.init(top: 80.0, left: 20.0, bottom: 100.0, right: 20.0), animated: true)
            }
        }
    }
    
    // MARK: - MapKit delegate
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
         let renderer = MKPolylineRenderer(overlay: overlay)
         renderer.strokeColor = UIColor.blue
         renderer.lineWidth = 2.0
         return renderer
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        guard annotation is SchoolAnn else {
            return nil
        }

        let identifier = "School"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
        if annotationView == nil {
            annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            annotationView?.canShowCallout = true
            let btn = UIButton(type: .detailDisclosure)
            annotationView?.rightCalloutAccessoryView = btn
        } else {
            annotationView?.annotation = annotation
        }

        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        guard let schoolAnnotation = view.annotation as? SchoolAnn else { return }
        let placeName = schoolAnnotation.title
        let placeInfo = schoolAnnotation.info

        let ac = UIAlertController(title: placeName, message: placeInfo, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
    }
}

