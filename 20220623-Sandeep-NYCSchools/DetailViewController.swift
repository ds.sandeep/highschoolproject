//
//  ViewController.swift
//  20220623-Sandeep-NYCSchools
//
//  Created by Sandeep on 6/23/22.
//

import UIKit
import MessageUI



class DetailViewController: BaseViewController, MFMailComposeViewControllerDelegate {

    
    
    @IBOutlet weak var backbtnIb: UIButton!
    @IBOutlet weak var directionBtnIb: UIButton!
    @IBOutlet weak var subwayBtnIb: UIButton!
    @IBOutlet weak var busBtnIb: UIButton!
    @IBOutlet weak var schoolNameIb: UILabel!
    @IBOutlet weak var schoolPhoneBtnIb: UIButton!
    @IBOutlet weak var schoolEmailBtnIb: UIButton!
    @IBOutlet weak var schoolwebbtnIb: UIButton!
    @IBOutlet weak var schoolAddrlblIb: UILabel!
    @IBOutlet weak var satreadinglblIb: UILabel!
    @IBOutlet weak var satmathlblIb: UILabel!
    @IBOutlet weak var writinglblIb: UILabel!
    @IBOutlet weak var sattakersIb: UILabel!
    @IBOutlet weak var satScoreStackIb: UIStackView!
    @IBOutlet weak var satscoreHeadingIb: UILabel!
    @IBOutlet weak var phontstackViewIb: UIStackView!
    @IBOutlet weak var emailstackViewIb: UIStackView!
    @IBOutlet weak var webstackViewIb: UIStackView!
    @IBOutlet weak var detailStackViewHeightIb: NSLayoutConstraint!
    
    
    
    var schoolDetaildata = [String:Any]()
    var schoolNamestr = ""
    var schoolNeighborhoodstr = ""
    var schoolphonestr = ""
    var schoolemailstr = ""
    var schoolwebstr = ""
    var schooladdr1str = ""
    var schoolcitystr = ""
    var schoolstatestr = ""
    var schoolzipstr = ""
    var schooldbnstr = ""
    var schoolreadavgstr = ""
    var schoolwritingavgstr = ""
    var schoolmathavgstr = ""
    var schooltesttakersstr = ""
    var schoollatstr = ""
    var schoollongstr = ""
    var schoolbusroute = ""
    var schoolsubwayrt = ""
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        guard let satScoredata = AppData.shared.satscoredata else {
            return
        }
        
        self.schoolPhoneBtnIb.contentHorizontalAlignment = .left
        self.schoolEmailBtnIb.contentHorizontalAlignment = .left
        self.schoolwebbtnIb.contentHorizontalAlignment = .left
        
        for eachData in schoolDetaildata {
            let key = eachData.key
            if key == "school_name" {
                schoolNamestr = eachData.value as? String ?? ""
            } else if key == "school_email" {
                schoolemailstr = eachData.value as? String ?? ""
            } else if key == "phone_number" {
                schoolphonestr = eachData.value as? String ?? ""
            } else if key == "website" {
                schoolwebstr = eachData.value as? String ?? ""
            } else if key == "primary_address_line_1" {
                schooladdr1str = eachData.value as? String ?? ""
            } else if key == "city" {
                schoolcitystr = eachData.value as? String ?? ""
            } else if key == "state_code" {
                schoolstatestr = eachData.value as? String ?? ""
            } else if key == "zip" {
                schoolzipstr = eachData.value as? String ?? ""
            } else if key == "dbn" {
                schooldbnstr = eachData.value as? String ?? ""
            } else if key == "latitude" {
                schoollatstr = eachData.value as? String ?? ""
            } else if key == "longitude" {
                schoollongstr = eachData.value as? String ?? ""
            } else if key == "neighborhood" {
                schoolNeighborhoodstr = eachData.value as? String ?? ""
            } else if key == "bus" {
                schoolbusroute = eachData.value as? String ?? ""
            } else if key == "subway" {
                schoolsubwayrt = eachData.value as? String ?? ""
            }
        }
        
        var foundsat = false
        for (_, eachItem) in satScoredata.enumerated() {
            let dbn = eachItem.dbn
            if dbn.caseInsensitiveCompare(schooldbnstr) == .orderedSame {
                schoolreadavgstr = eachItem.satCriticalReadingAvgScore
                schoolwritingavgstr = eachItem.satWritingAvgScore
                schoolmathavgstr = eachItem.satMathAvgScore
                schooltesttakersstr = eachItem.numOfSatTestTakers
                foundsat = true
            }
        }
        
        self.schoolNameIb.text = schoolNamestr
        if schoolemailstr.isEmpty {
            self.emailstackViewIb.isHidden = true
            self.detailStackViewHeightIb.constant = 62
        } else {
            self.schoolEmailBtnIb.setTitle("\(schoolemailstr)", for: UIControl.State.normal)
            self.detailStackViewHeightIb.constant = 83
        }
        self.schoolPhoneBtnIb.setTitle("\(schoolphonestr)", for: UIControl.State.normal)
        self.schoolwebbtnIb.setTitle("\(schoolwebstr)", for: UIControl.State.normal)
        let addressValue = "\(schooladdr1str), \(schoolcitystr), \(schoolstatestr) \(schoolzipstr)"
        self.schoolAddrlblIb.text = addressValue
        
        if foundsat {
            self.satscoreHeadingIb.text = "SAT Score"
            self.satreadinglblIb.text = schoolreadavgstr
            self.writinglblIb.text = schoolwritingavgstr
            self.satmathlblIb.text = schoolmathavgstr
            self.sattakersIb.text = schooltesttakersstr
        } else {
            self.satScoreStackIb.isHidden = true
            self.satscoreHeadingIb.text = "SAT Score not available."
        }
        
    }
    
    // MARK: - back button action
    @IBAction func backButtonAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    //MARK: - Visit website button Action
    @IBAction func visitWebsiteBtnAction(_ sender: UIButton) {
        if let schoolweb = URL(string: "https://\(schoolwebstr)") {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(schoolweb, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(schoolweb)
            }
        }
    }
    
    //MARK: - school email button Action
    @IBAction func emailBtnAction(_ sender: UIButton) {
        
        if !schoolemailstr.isEmpty {
            let recipientEmail = schoolemailstr
            
            // Show default mail composer
            if MFMailComposeViewController.canSendMail() {
                let mail = MFMailComposeViewController()
                mail.mailComposeDelegate = self
                mail.setToRecipients([recipientEmail])
                
                present(mail, animated: true)
            
            // Show third party email composer if default Mail app is not present
            } else if let emailUrl = createEmailUrl(to: recipientEmail, subject: "", body: "") {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(emailUrl, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(emailUrl)
                }
            }
        }
    }
    
    //MARK: - school direction button action
    @IBAction func schoolDirectionButtonAction(_ sender: Any) {
        let mainBoard = UIStoryboard(name: "Main", bundle: nil)
        let schoolVC = mainBoard.instantiateViewController(withIdentifier: "schoolDirectionScene") as! SchoolDirection
        schoolVC.schoolNamestr = self.schoolNamestr
        schoolVC.schoolNeighborhoodstr = self.schoolNeighborhoodstr
        schoolVC.schoollatstr = self.schoollatstr
        schoolVC.schoollongstr = self.schoollongstr
        schoolVC.modalPresentationStyle = .fullScreen
        self.topMostController().present(schoolVC, animated: true, completion: nil)
    }
    
    //MARK: - Bus / Subway route info button action
    @IBAction func routeInfoButtonAction(_ sender: UIButton) {
        
        var routeInfostr = ""
        let tag = sender.tag
        
        if tag == 1 {
            routeInfostr = schoolsubwayrt
        } else {
            routeInfostr = schoolbusroute
        }
        
        let routeVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "routeInfoScene") as! RouteInfoVC
        routeVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        routeVC.modalTransitionStyle = .crossDissolve
        routeVC.definesPresentationContext = true
        routeVC.routeInfostr = routeInfostr
        present(routeVC, animated: true, completion: nil)
    }
    
    //MARK: - school phone button action
    @IBAction func schoolphoneButtonAction(_ sender: Any) {
        if let phoneNumber = self.schoolPhoneBtnIb.titleLabel?.text {
            var phoneNum = phoneNumber.replacingOccurrences(of: "(", with: "")
            phoneNum = phoneNum.replacingOccurrences(of: ")", with: "")
            phoneNum = phoneNum.replacingOccurrences(of: " ", with: "")
            phoneNum = phoneNum.replacingOccurrences(of: "-", with: "")
            callNumber(phoneNumber: phoneNum);
        }
    }
    
    private func createEmailUrl(to: String, subject: String, body: String) -> URL? {
        let subjectEncoded = subject.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        let bodyEncoded = body.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        
        let gmailUrl = URL(string: "googlegmail://co?to=\(to)&subject=\(subjectEncoded)&body=\(bodyEncoded)")
        let outlookUrl = URL(string: "ms-outlook://compose?to=\(to)&subject=\(subjectEncoded)")
        //let defaultUrl = URL(string: "mailto:\(to)?subject=\(subjectEncoded)&body=\(bodyEncoded)")
        let defaultUrl = URL(string: "mailto:\(to)")
        
        if let gmailUrl = gmailUrl, UIApplication.shared.canOpenURL(gmailUrl) {
            return gmailUrl
        } else if let outlookUrl = outlookUrl, UIApplication.shared.canOpenURL(outlookUrl) {
            return outlookUrl
        }
        
        return defaultUrl
    }
    
    //MARK: - MailComposer delegate method
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
    //MARK: - call number method
    private func callNumber(phoneNumber:String) {
        if let phoneCallURL: URL = URL(string: "telprompt://\(phoneNumber)") {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                application.open(phoneCallURL, options: [:], completionHandler: nil)
            }
      }
    }
}

