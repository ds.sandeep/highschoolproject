//
//  BaseViewController.swift
//  SmartApp
//

import Foundation
import UIKit




class BaseViewController : UIViewController {
    
    @IBOutlet weak var keyboardHeightLayoutConstraint: NSLayoutConstraint!
    
    var hasTopNotch: Bool {
        if #available(iOS 11.0, tvOS 11.0, *) {
            return UIApplication.shared.delegate?.window??.safeAreaInsets.top ?? 0 > 20
        }
        return false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if isiPad() {
            return .all
        } else {
            return .portrait
        }
    }
    
    func isiPad() -> Bool {
        if UIDevice.current.userInterfaceIdiom == .pad {
            return true
        }
        return false
    }
    
    //MARK: - Show single button alert
    func displayAlert(dispmessage: String) {
        self.displayAlert(withTitle: UtilityDataEnum.AppName, dispmessage: dispmessage, completion: {})
    }
    
    func displayAlert(withTitle title:String, dispmessage: String, completion: @escaping () -> Void) {
        
        let alert = UIAlertController(title: title, message: dispmessage, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (alertAction: UIAlertAction) in
            completion()
        }))
        
        UIApplication.topViewController()!.present(alert, animated: true, completion: nil)
    }
    
    func topMostController() -> UIViewController {
        
        var topController: UIViewController = UIApplication.shared.keyWindow!.rootViewController!
        while (topController.presentedViewController != nil) {
            topController = topController.presentedViewController!
        }
        return topController
    }
    
    deinit {
        
    }

}


extension UIApplication {
    
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}
