//
//  RouteInfoVC.swift
//
//

import UIKit





class RouteInfoVC: BaseViewController {
    
    
    @IBOutlet weak var contentViewIb: UIView!
    @IBOutlet weak var closeBtnIb: UIButton!
    @IBOutlet weak var routelblIb: UILabel!
    
    var routeInfostr = ""
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.view.backgroundColor = UIColor.clear
        //view.isOpaque = false
        
        let blurEffect = UIBlurEffect(style: .dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.view.frame
        //blurEffectView.alpha = 35.0

        self.view.insertSubview(blurEffectView, at: 0)
        
        self.routelblIb.text = routeInfostr
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    // MARK: - back button action
    @IBAction func closeActionButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

