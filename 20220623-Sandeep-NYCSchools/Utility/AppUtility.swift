//
//  AppUtility.swift
//

import UIKit
import LocalAuthentication





enum UtilityDataEnum {
    
    static let AppName = "HighSchoolApp"
    static var serviceURL = "https://data.cityofnewyork.us/resource/"
    static var highSchoolURL = "s3k6-pzi2.json"
    static var satScoreURL = "f9bf-2cp4.json"
}




class AppUtility {
    
    
    
    static let shared = AppUtility()
    
    
    func isiPad() -> Bool {
        if UIDevice.current.userInterfaceIdiom == .pad {
            return true
        }
        return false
    }
    
    func jsonToData(json: Any) -> Data? {
        do {
            return try JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions.prettyPrinted)
        } catch let myJSONError {
            print(myJSONError)
        }
        return nil
    }
    
    func formattedNumberVal(amount: Double) -> String {
        
        let unformatVal : Double = amount
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2 //change as desired
        formatter.locale = Locale(identifier: "en_US") // locale identifier codes: https://gist.github.com/jacobbubu/1836273
        let displayValue : String = formatter.string(from: NSNumber(value: unformatVal))! // displayValue: "$3,534,235" ```
        return displayValue
    }
    
}





