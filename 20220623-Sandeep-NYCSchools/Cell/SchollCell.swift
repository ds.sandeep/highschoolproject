//
//  SchollCell.swift
//  SmartApp
//

import Foundation
import UIKit




class SchollCell: UITableViewCell {
    

    @IBOutlet weak var schoolNamelblIb: UILabel!
    @IBOutlet weak var schoolPhoneIb: UILabel!
    @IBOutlet weak var schoolEmailIb: UILabel!
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

        let mScreenSize = UIScreen.main.bounds
        let mSeparatorHeight = CGFloat(1.0) // Change height of speatator as you want
        let mAddSeparator = UIView.init(frame: CGRect(x: 0, y: self.frame.size.height - mSeparatorHeight, width: mScreenSize.width, height: mSeparatorHeight))
        mAddSeparator.backgroundColor = UIColor.lightGray // Change backgroundColor of separator
        self.addSubview(mAddSeparator)
    }
    
}
