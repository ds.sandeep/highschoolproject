//
//  Data.swift
//  SmartApp
//

import Foundation

class AppData: NSObject {
    
    @objc static let shared = AppData()
    
    private override init() {
    }
    
    //high school data
    var highschooldata:highSchoolModel?
    
    //high school SAT score data
    var satscoredata: satScoredata?
}
