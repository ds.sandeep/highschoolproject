//
//  SplashVC.swift
//  20220623-Sandeep-NYCSchools
//
//  Created by Sandeep on 6/23/22.
//

import UIKit
import Foundation



class SplashVC: BaseViewController {

    
    @IBOutlet weak var retryBtnIb: UIButton!
    
    
    var schoolData = [[String:Any]]()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.fetchHighSchoolSATData()
    }

    
    // MARK: - retry button action
    @IBAction func retryActionButton(_ sender: Any) {
        self.fetchHighSchoolSATData()
    }
    
    // MARK: - fetch High School Data
    func fetchHighSchoolData() {
        
        self.showIndicator(withTitle: "Please wait", and: "")
        
        WebSerivceHelperClass.fetchHighSchoolData{ [weak self] (success, highschoolData, message) in
            guard let weakself = self else {
                return
            }
            if success == true {
                weakself.retryBtnIb.isHidden = true
                AppData.shared.highschooldata = highschoolData
                if let highschoolitems = highschoolData {
                    for eachItem in highschoolitems {
                        var data = [String: Any]()
                        for eachData in eachItem {
                            let key = eachData.key
                            if key == "school_name" {
                                data[eachData.key] = eachData.value
                            } else if key == "school_email" {
                                data[eachData.key] = eachData.value
                            } else if key == "phone_number" {
                                data[eachData.key] = eachData.value
                            } else if key == "website" {
                                data[eachData.key] = eachData.value
                            } else if key == "primary_address_line_1" {
                                data[eachData.key] = eachData.value
                            } else if key == "city" {
                                data[eachData.key] = eachData.value
                            } else if key == "state_code" {
                                data[eachData.key] = eachData.value
                            } else if key == "zip" {
                                data[eachData.key] = eachData.value
                            } else if key == "dbn" {
                                data[eachData.key] = eachData.value
                            } else if key == "latitude" {
                                data[eachData.key] = eachData.value
                            } else if key == "longitude" {
                                data[eachData.key] = eachData.value
                            } else if key == "neighborhood" {
                                data[eachData.key] = eachData.value
                            } else if key == "subway" {
                                data[eachData.key] = eachData.value
                            } else if key == "bus" {
                                data[eachData.key] = eachData.value
                            }
                        }
                        weakself.schoolData.append(data)
                    }
                }
                weakself.hideIndicator()
                
                let mainBoard = UIStoryboard(name: "Main", bundle: nil)
                let viewVC = mainBoard.instantiateViewController(withIdentifier: "viewControllerScene") as! ViewController
                viewVC.schoolData = weakself.schoolData
                viewVC.modalPresentationStyle = .fullScreen
                weakself.topMostController().present(viewVC, animated: true, completion: nil)
                
            } else {
                weakself.hideIndicator()
                weakself.displayAlert(dispmessage: message)
                weakself.retryBtnIb.isHidden = false
            }
        }
    }
    
    // MARK: - fetch High School SAT score
    func fetchHighSchoolSATData() {
        
        self.showIndicator(withTitle: "Please wait", and: "")
        
        WebSerivceHelperClass.fetchSATScoreData{ [weak self] (success, satScoreData, message) in
            guard let weakself = self else {
                return
            }
            if success == true {
                weakself.retryBtnIb.isHidden = true
                AppData.shared.satscoredata = satScoreData
                weakself.hideIndicator()
                
                weakself.fetchHighSchoolData()
            } else {
                weakself.hideIndicator()
                weakself.displayAlert(dispmessage: message)
                weakself.retryBtnIb.isHidden = false
            }
        }
    }
}

