//
//  _0220623_Sandeep_NYCSchoolsTests.swift
//  20220623-Sandeep-NYCSchoolsTests
//

import XCTest
@testable import _0220623_Sandeep_NYCSchools



class _0220623_Sandeep_NYCSchoolsTests: XCTestCase {
    
    var satScoreItem: SATScoreModel!
    var highSchoolItem = highSchoolModel()
      
      override func setUp() {
          satScoreItem = SATScoreModel(dbn: "01M292", schoolName: "HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES", numOfSatTestTakers: "29", satCriticalReadingAvgScore: "355", satMathAvgScore: "404", satWritingAvgScore: "363")
          
          var testDic = [String: String]()
          testDic["dbn"] = "01M292"
          testDic["school_name"] = "HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES"
          highSchoolItem.append(testDic)
      }
      
      override func tearDown() {
          satScoreItem = nil
          highSchoolItem.removeAll()
      }
    
    func testSatScoreModel() {
        
        XCTAssertEqual(satScoreItem.dbn, "01M292")
        XCTAssertEqual(satScoreItem.schoolName, "HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES")
        XCTAssertEqual(satScoreItem.numOfSatTestTakers, "29")
        XCTAssertEqual(satScoreItem.satCriticalReadingAvgScore, "355")
        XCTAssertEqual(satScoreItem.satMathAvgScore, "404")
        XCTAssertEqual(satScoreItem.satWritingAvgScore, "363")
    }
    
    func testHighSchoolModel() {
        XCTAssert(highSchoolItem.count > 0)
    }

}



